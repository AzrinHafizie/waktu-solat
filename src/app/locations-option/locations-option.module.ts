import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LocationsOptionPageRoutingModule } from './locations-option-routing.module';

import { LocationsOptionPage } from './locations-option.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LocationsOptionPageRoutingModule
  ],
  declarations: [LocationsOptionPage]
})
export class LocationsOptionPageModule {}
