import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LocationsOptionPage } from './locations-option.page';

const routes: Routes = [
  {
    path: '',
    component: LocationsOptionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LocationsOptionPageRoutingModule {}
