import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController } from '@ionic/angular';

import { LocationService } from './../services/location.service';
import { JakimService } from './../services/jakim.service';

@Component({
  selector: 'app-locations-option',
  templateUrl: './locations-option.page.html',
  styleUrls: ['./locations-option.page.scss'],
})
export class LocationsOptionPage implements OnInit {

  locations = [
    {
      province: 'JHR01 - Pulau Aur dan Pulau Pemanggil ',
      state: 'Johor',
      code: 'JHR01'
    },
    {
      province: 'JHR02 - Johor Bahru, Kota Tinggi, Mersing',
      state: 'Johor',
      code: 'JHR02'
    },
    {
      province: 'JHR03 - Kluang, Pontian',
      state: 'Johor',
      code: 'JHR03'
    },
    {
      province: 'JHR04 - Batu Pahat, Muar, Segamat, Gemas Johor',
      state: 'Johor',
      code: 'JHR04'
    },
    {
      province: 'KDH01 - Kota Setar, Kubang Pasu, Pokok Sena (Daerah Kecil)',
      state: 'Kedah',
      code: 'KDH01'
    },
    {
      province: 'KDH02 - Kuala Muda, Yan, Pendang',
      state: 'Kedah',
      code: 'KDH02'
    },
    {
      province: 'KDH03 - Padang Terap, Sik',
      state: 'Kedah',
      code: 'KDH03'
    },
    {
      province: 'KDH04 - Baling',
      state: 'Kedah',
      code: 'KDH04'
    },
    {
      province: 'KDH05 - Bandar Baharu, Kulim',
      state: 'Kedah',
      code: 'KDH05'
    },
    {
      province: 'KDH06 - Langkawi',
      state: 'Kedah',
      code: 'KDH06'
    },
    {
      province: 'KDH07 - Puncak Gunung Jerai',
      state: 'Kedah',
      code: 'KDH07'
    },
    {
      province: 'KTN01 - Bachok, Kota Bharu, Machang, Pasir Mas, Pasir Puteh, Tanah Merah, Tumpat, Kuala Krai, Mukim Chiku',
      state: 'Kelantan',
      code: 'KTN01'
    },
    {
      province: 'KTN03 - Gua Musang (Daerah Galas Dan Bertam), Jeli',
      state: 'Kelantan',
      code: 'KTN03'
    },
    {
      province: 'MLK01 - SELURUH NEGERI MELAKA',
      state: 'Melaka',
      code: 'MLK01'
    },
    {
      province: 'NGS01 - Tampin, Jempol',
      state: 'Negeri Sembilan',
      code: 'NGS01'
    },
    {
      province: 'NGS02 - Jelebu, Kuala Pilah, Port Dickson, Rembau, Seremban',
      state: 'Negeri Sembilan',
      code: 'NGS02'
    },
    {
      province: 'PHG01 - Pulau Tioman',
      state: 'Pahang',
      code: 'PHG01'
    },
    {
      province: 'PHG02 - Kuantan, Pekan, Rompin, Muadzam Shah',
      state: 'Pahang',
      code: 'PHG02'
    },
    {
      province: 'PHG03 - Jerantut, Temerloh, Maran, Bera, Chenor, Jengka',
      state: 'Pahang',
      code: 'PHG03'
    },
    {
      province: 'PHG04 - Bentong, Lipis, Raub',
      state: 'Pahang',
      code: 'PHG04'
    },
    {
      province: 'PHG05 - Genting Sempah, Janda Baik, Bukit Tinggi',
      state: 'Pahang',
      code: 'PHG05'
    },
    {
      province: 'PHG06 - Cameron Highlands, Genting Higlands, Bukit Fraser',
      state: 'Pahang',
      code: 'PHG06'
    },
    {
      province: 'PLS01 - Kangar, Padang Besar, Arau',
      state: 'Perlis',
      code: 'PLS01'
    },
    {
      province: 'PNG01 - Seluruh Negeri Pulau Pinang',
      state: 'Pulau Pinang',
      code: 'PNG01'
    },
    {
      province: 'PRK01 - Tapah, Slim River, Tanjung Malim',
      state: 'Perak',
      code: 'PRK01'
    },
    {
      province: 'PRK02 - Kuala Kangsar, Sg. Siput (Daerah Kecil), Ipoh, Batu Gajah, Kampar',
      state: 'Perak',
      code: 'PRK02'
    },
    {
      province: 'PRK03 - Lenggong, Pengkalan Hulu, Grik',
      state: 'Perak',
      code: 'PRK03'
    },
    {
      province: 'PRK04 - Temengor, Belum',
      state: 'Perak',
      code: 'PRK04'
    },
    {
      province: 'PRK05 - Kg Gajah, Teluk Intan, Bagan Datuk, Seri Iskandar, Beruas, Parit, Lumut, Sitiawan, Pulau Pangkor',
      state: 'Perak',
      code: 'PRK05'
    },
    {
      province: 'PRK06 - Selama, Taiping, Bagan Serai, Parit Buntar',
      state: 'Perak',
      code: 'PRK06'
    },
    {
      province: 'PRK07 - Bukit Larut',
      state: 'Perak',
      code: 'PRK07'
    },
    {
      province: 'SBH01 - Bahagian Sandakan (Timur), Bukit Garam, Semawang, Temanggong, Tambisan, Bandar Sandakan, Sukau',
      state: 'Sabah',
      code: 'SBH01'
    },
    {
      province: 'SBH02 - Beluran, Telupid, Pinangah, Terusan, Kuamut, Bahagian Sandakan (Barat)',
      state: 'Sabah',
      code: 'SBH02'
    },
    {
      province: 'SBH03 - Lahad Datu, Silabukan, Kunak, Sahabat, Semporna, Tungku, Bahagian Tawau  (Timur)',
      state: 'Sabah',
      code: 'SBH03'
    },
    {
      province: 'SBH04 - Bandar Tawau, Balong, Merotai, Kalabakan, Bahagian Tawau (Barat)',
      state: 'Sabah',
      code: 'SBH04'
    },
    {
      province: 'SBH05 - Kudat, Kota Marudu, Pitas, Pulau Banggi, Bahagian Kudat',
      state: 'Sabah',
      code: 'SBH05'
    },
    {
      province: 'SBH06 - Gunung Kinabalu',
      state: 'Sabah',
      code: 'SBH06'
    },
    {
      province: 'SBH07 - Kota Kinabalu, Ranau, Kota Belud, Tuaran, Penampang, Papar, Putatan, Bahagian Pantai Barat',
      state: 'Sabah',
      code: 'SBH07'
    },
    {
      province: 'SBH08 - Pensiangan, Keningau, Tambunan, Nabawan, Bahagian Pendalaman (Atas)',
      state: 'Sabah',
      code: 'SBH08'
    },
    {
      province: 'SBH09 - Beaufort, Kuala Penyu, Sipitang, Tenom, Long Pa Sia, Membakut, Weston, Bahagian Pendalaman (Bawah)',
      state: 'Sabah',
      code: 'SBH09'
    },
    {
      province: 'SGR01 - Gombak, Petaling, Sepang, Hulu Langat, Hulu Selangor, S.Alam',
      state: 'Selangor',
      code: 'SGR01'
    },
    {
      province: 'SGR02 - Kuala Selangor, Sabak Bernam',
      state: 'Selangor',
      code: 'SGR02'
    },
    {
      province: 'SGR03 - Klang, Kuala Langat',
      state: 'Selangor',
      code: 'SGR03'
    },
    {
      province: 'SWK01 - Limbang, Lawas, Sundar, Trusan',
      state: 'Sarawak',
      code: 'SWK01'
    },
    {
      province: 'SWK02 - Miri, Niah, Bekenu, Sibuti, Marudi',
      state: 'Sarawak',
      code: 'SWK02'
    },
    {
      province: 'SWK03 - Pandan, Belaga, Suai, Tatau, Sebauh, Bintulu',
      state: 'Sarawak',
      code: 'SWK03'
    },
    {
      province: 'SWK04 - Sibu, Mukah, Dalat, Song, Igan, Oya, Balingian, Kanowit, Kapit',
      state: 'Sarawak',
      code: 'SWK04'
    },
    {
      province: 'SWK05 - Sarikei, Matu, Julau, Rajang, Daro, Bintangor, Belawai',
      state: 'Sarawak',
      code: 'SWK05'
    },
    {
      province: 'SWK06 - Lubok Antu, Sri Aman, Roban, Debak, Kabong, Lingga, Engkelili, Betong, Spaoh, Pusa, Saratok',
      state: 'Sarawak',
      code: 'SWK06'
    },
    {
      province: 'SWK07 - Serian, Simunjan, Samarahan, Sebuyau, Meludam',
      state: 'Sarawak',
      code: 'SWK07'
    },
    {
      province: 'SWK08 - Kuching, Bau, Lundu, Sematan',
      state: 'Sarawak',
      code: 'SWK08'
    },
    {
      province: 'SWK09 - Zon Khas (Kampung Patarikan)',
      state: 'Sarawak',
      code: 'SWK09'
    },
    {
      province: 'TRG01 - Kuala Terengganu, Marang, Kuala Nerus',
      state: 'Terengganu',
      code: 'TRG01'
    },
    {
      province: 'TRG02 - Besut, Setiu',
      state: 'Terengganu',
      code: 'TRG02'
    },
    {
      province: 'TRG03 - Hulu Terengganu',
      state: 'Terengganu',
      code: 'TRG03'
    },
    {
      province: 'TRG04 - Dungun, Kemaman',
      state: 'Terengganu',
      code: 'TRG04'
    },
    {
      province: 'WLY01 - Kuala Lumpur, Putrajaya',
      state: 'Wilayah Persekutuan',
      code: 'WLY01'
    },
    {
      province: 'WLY02 - Labuan',
      state: 'Wilayah Persekutuan',
      code: 'WLY02'
    }
  ];

  filteredLocation = [];
  groupedState = [];

  fetchingYearlyData = false;
  loading;

  constructor(
    private navController: NavController,
    private locationService: LocationService,
    private jakimService: JakimService,
    private loadingController: LoadingController
  ) { }

  ngOnInit() {
    // this.filteredLocation = this.locations
    this.groupByState();
  }

  groupByState(){
    let loc: any = this.locations;
    loc = loc.map(m=>{
      const str = m.code.slice(0,3);
      return str;
    });
    loc = loc.reduce((acc, curr)=>{
        if (acc.indexOf(curr) === -1) {
        acc.push(curr);
      }
      return acc;
    }, []);

    let group = [];

    group = loc.map(m=>{
      const place = this.locations.filter(f=>{
        if (f.code.slice(0,3) === m){
          return f;
        }
      });
      const obj = {
        state: place[0].state,
        zone: place
      };
      return obj;
    });

    this.groupedState = [...group];
    this.filteredLocation = [...this.groupedState];

  }

  setLocation(code) {
    const loc = this.locations.filter(f=>f.code === code);
    const newLoc = {
      ...loc[0],
      date: new Date()
    };
    this.locationService.setLocation(newLoc);

    this.getWaktuByLocation(newLoc);

  }

  async getWaktuByLocation(loc){
    this.fetchingYearlyData = true;

    this.loading = await this.loadingController.create({
      message: 'Please wait...',
    });
    this.loading.present();

    this.jakimService.getYearly(loc.code).subscribe(res=>{
      this.fetchingYearlyData = false;
      this.loading.dismiss();
      this.navController.pop();
    },
    (err)=> {
      this.fetchingYearlyData = false;
      this.loading.dismiss();
    });
  }

  findLocation(e: any) {
    const key = e.detail.value;

    let arr = [];
    this.groupedState.forEach(f=>{
      const m = f.zone.filter(x=>{
        if(x.province.toLowerCase().includes(key.toLowerCase())){
          return x;
        }
      });
      const obj = {
        state: f.state,
        zone: m
      };
      if (m.length>0){
        arr = [...arr, obj];
      }

    });

    this.filteredLocation = [...arr];

  }

}
