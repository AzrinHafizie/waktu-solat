import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

import { LocationService } from './../services/location.service';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.page.html',
  styleUrls: ['./setting.page.scss'],
})
export class SettingPage implements OnInit {

  location$ = this.locationService.location$;

  constructor(
    private navController: NavController,
    private locationService: LocationService
    ) { }

  ngOnInit() {
    this.locationService.selectedLocation();
  }

  showLocationOption(){
    this.navController.navigateForward(['locations-option']);
  }

}
