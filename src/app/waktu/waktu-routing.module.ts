import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WaktuPage } from './waktu.page';

const routes: Routes = [
  {
    path: '',
    component: WaktuPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WaktuPageRoutingModule {}
