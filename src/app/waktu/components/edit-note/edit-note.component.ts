import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { Platform } from '@ionic/angular';

import { NotesService } from './../../../services/notes.service';

@Component({
  selector: 'app-edit-note',
  templateUrl: './edit-note.component.html',
  styleUrls: ['./edit-note.component.scss'],
})
export class EditNoteComponent implements OnInit {

  @Input() noteData: any;

  noteValue = '';
  web = false;
  selectedImg;
  userNote = false;

  constructor(
    public modalController: ModalController,
    private notesService: NotesService,
    public platform: Platform
  ) { }

  ngOnInit() {

    if (this.noteData.value) {
      this.noteValue = this.noteData.value;
    }

    if (!this.noteData.type) {
      this.userNote = true;
    }

    if (this.noteData.img) {
      this.selectedImg = this.noteData.img;
    }

    let arr = this.platform.platforms();
    arr = arr.filter(f=>{
      if (f === 'mobileweb') {
        return f;
      }
    });
    if (arr.length > 0) {
      this.web = true;
    } else {
      this.web = false;
    }

  }

  getImage(val) {
    if (this.web){
      val.click();
    } else {
      //shud call native
    }
  }

  displayImage(e){
    const reader = new FileReader();
    reader.onload = (ev) => {
      this.selectedImg = ev.target.result;
    };
    reader.readAsDataURL(e.target.files[0]);
  }

  clearImg(){
    this.selectedImg = undefined;
  }

  saveNotes(){

    const obj = {
      value: this.noteValue,
      date: new Date(),
      img: this.selectedImg
    };

    this.notesService.editNotes({
      ...obj
    },
    this.noteData);
    this.dismissModal();
  }

  dismissModal() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      dismissed: true
    });
  }

}
