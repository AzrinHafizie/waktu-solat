import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { Platform } from '@ionic/angular';

import { NotesService } from './../../../services/notes.service';

@Component({
  selector: 'app-create-note',
  templateUrl: './create-note.component.html',
  styleUrls: ['./create-note.component.scss'],
})
export class CreateNoteComponent implements OnInit {

  noteValue = '';
  web = false;
  selectedImg;

  constructor(
    public modalController: ModalController,
    private notesService: NotesService,
    public platform: Platform
  ) { }

  ngOnInit() {
    let arr = this.platform.platforms();
    arr = arr.filter(f=>{
      if (f === 'mobileweb') {
        return f;
      }
    });
    if (arr.length > 0) {
      this.web = true;
    } else {
      this.web = false;
    }

  }

  getImage(val) {
    if (this.web){
      val.click();
    } else {
      //shud call native
    }
  }

  displayImage(e){
    const reader = new FileReader();
    reader.onload = (ev) => {
      this.selectedImg = ev.target.result;
    };
    reader.readAsDataURL(e.target.files[0]);
  }

  clearImg(){
    this.selectedImg = undefined;
  }

  saveNotes(){

    const obj = {
      value: this.noteValue,
      date: new Date(),
      img: this.selectedImg
    };

    this.notesService.createNotes({
      ...obj
    });
    this.dismissModal();
  }

  dismissModal() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      dismissed: true
    });
  }

}
