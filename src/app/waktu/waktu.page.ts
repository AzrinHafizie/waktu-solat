import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

import { JakimService } from './../services/jakim.service';
import { LocationService } from './../services/location.service';

import { intervalToDuration, formatDistanceToNow, parseISO, format } from 'date-fns';
import { ms } from 'date-fns/locale';

import { ModalController } from '@ionic/angular';
import { CreateNoteComponent } from './components/create-note/create-note.component';
import { EditNoteComponent } from './components/edit-note/edit-note.component';

import { NotesService } from './../services/notes.service';

import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-waktu',
  templateUrl: './waktu.page.html',
  styleUrls: ['./waktu.page.scss'],
})
export class WaktuPage implements OnInit {

  location$ = this.locationService.location$;
  schedule$ = this.jakimService.yearlySchedule$;
  notes$ = this.notesService.notes$;

  time = new Date();

  remainingTime;
  elapsedTime;

  currentWaktu = {
    name: '',
    time: new Date()
  };

  nextWaktu = {
    name: '',
    time: new Date()
  };

  jadual = {
    date: new Date(),
    imsak: new Date(),
    subuh: new Date(),
    zohor: new Date(),
    asar: new Date(),
    maghrib: new Date(),
    isya: new Date()
  };

  jadualEsok = {
    date: new Date(),
    imsak: new Date(),
    subuh: new Date(),
    zohor: new Date(),
    asar: new Date(),
    maghrib: new Date(),
    isya: new Date()
  };

  jadualSemalam = {
    date: new Date(),
    imsak: new Date(),
    subuh: new Date(),
    zohor: new Date(),
    asar: new Date(),
    maghrib: new Date(),
    isya: new Date()
  };

  sytemNotes = [];
  combinedNotes = [];

  constructor(
    private jakimService: JakimService,
    private locationService: LocationService,
    private navController: NavController,
    public modalController: ModalController,
    public notesService: NotesService,
    public actionSheetController: ActionSheetController
  ) { }

  ngOnInit() {

    this.checkCurrentTime();

    this.location$.subscribe((res: any)=>{
      if (res && res.code){
        // this.jakimService.getYearly(res.code).subscribe()
      } else {
        this.navController.navigateForward(['locations-option']);
      }
    });

    this.locationService.selectedLocation();


    this.schedule$.subscribe(res=>{
      if(res && res.prayerTime && res.prayerTime.length > 0){
        this.compareDate(res.prayerTime);
      }
    });

    // this.notes$.subscribe()

    this.location$.subscribe(res=>{
      if(res && res.state){
        const lokasiNote = `Lokasi pilihan anda adalah ${ res.province }, ${ res.state }`;
        this.newSystemNote('lokasi', lokasiNote, res.date);
      }
    });

  }

  stringToMonth(str){
    let month = '';
    switch (str) {
      case 'Jan':
        month = '00';
        break;
      case 'Feb':
        month = '01';
        break;
      case 'Mac':
        month = '02';
        break;
      case 'Apr':
        month = '03';
        break;
      case 'Mei':
        month = '04';
        break;
      case 'Jun':
        month = '05';
        break;
      case 'Jul':
        month = '06';
        break;
      case 'Ogos':
        month = '07';
        break;
      case 'Sep':
        month = '08';
        break;
      case 'Okt':
        month = '09';
      case 'Nov':
        month = '10';
        break;
      case 'Dis':
        month = '11';
        break;
      default:
        break;
    }
    return month;
  }

  compareDate(arr){
    arr = arr.map(m=>{

      if (m.date instanceof Date ){
        return m;
      } else {

        // let split = m.date.split('-')
        // let month:any = this.stringToMonth(split[1])
        // m.date = new Date(split[2], month, split[0])

        m.date = new Date(m.date);

        return m;
      }

    });

    const today = arr.filter(f => {
      if (this.time.toLocaleDateString() === f.date.toLocaleDateString() ) {
        return f;
      }
    });

    const tomorrow = arr.filter(f => {
      const date = new Date(this.time.getFullYear(), this.time.getMonth(), this.time.getDate());
      // console.log(this.time.toLocaleDateString(), new Date(date.setDate(date.getDate()+1)).toLocaleDateString() );
      if (
        new Date(date.setDate(date.getDate()+1)).toLocaleDateString() === f.date.toLocaleDateString()
      ) {
        return f;
      }
    });

    const yesterday = arr.filter(f => {
      const date = new Date(this.time.getFullYear(), this.time.getMonth(), this.time.getDate());
      if (
        new Date(date.setDate(date.getDate()-1)).toLocaleDateString() === f.date.toLocaleDateString()
      ) {
        return f;
      }
    });

    this.jadual.date = today[0].date;
    this.jadual.imsak = this.stringToDate(today[0].imsak);
    this.jadual.subuh = this.stringToDate(today[0].fajr);
    this.jadual.zohor = this.stringToDate(today[0].dhuhr);
    this.jadual.asar = this.stringToDate(today[0].asr);
    this.jadual.maghrib = this.stringToDate(today[0].maghrib);
    this.jadual.isya = this.stringToDate(today[0].isha);

    this.jadualEsok.date = tomorrow[0].date;
    this.jadualEsok.imsak = this.stringToDate(tomorrow[0].imsak, tomorrow[0].date);
    this.jadualEsok.subuh = this.stringToDate(tomorrow[0].fajr, tomorrow[0].date);
    this.jadualEsok.zohor = this.stringToDate(tomorrow[0].dhuhr, tomorrow[0].date);
    this.jadualEsok.asar = this.stringToDate(tomorrow[0].asr, tomorrow[0].date);
    this.jadualEsok.maghrib = this.stringToDate(tomorrow[0].maghrib, tomorrow[0].date);
    this.jadualEsok.isya = this.stringToDate(tomorrow[0].isha, tomorrow[0].date);

    this.jadualSemalam.date = yesterday[0].date;
    this.jadualSemalam.imsak = this.stringToDate(yesterday[0].imsak, yesterday[0].date);
    this.jadualSemalam.subuh = this.stringToDate(yesterday[0].fajr, yesterday[0].date);
    this.jadualSemalam.zohor = this.stringToDate(yesterday[0].dhuhr, yesterday[0].date);
    this.jadualSemalam.asar = this.stringToDate(yesterday[0].asr, yesterday[0].date);
    this.jadualSemalam.maghrib = this.stringToDate(yesterday[0].maghrib, yesterday[0].date);
    this.jadualSemalam.isya = this.stringToDate(yesterday[0].isha, yesterday[0].date);

    const jadualNote = ` Waktu Hari Ini,
    Imsak: ${ (this.jadual.imsak ? format(this.jadual.imsak, 'HH:mm') : '-')}
    Subuh: ${ (this.jadual.subuh ? format(this.jadual.subuh, 'HH:mm') : '-')}
    Zohor: ${ (this.jadual.zohor ? format(this.jadual.zohor, 'HH:mm') : '-')}
    Asar: ${ (this.jadual.asar ? format(this.jadual.asar, 'HH:mm') : '-')}
    Maghrib: ${ (this.jadual.maghrib ? format(this.jadual.maghrib, 'HH:mm') : '-')}
    Isya: ${ (this.jadual.isya ? format(this.jadual.isya, 'HH:mm') : '-')}`;

    this.newSystemNote('waktu', jadualNote, new Date().setHours(0, 0, 0) );

  }

  newSystemNote(type, value, date) {
    const obj = {
      createdBy: 'ayuh app',
      type,
      date: new Date(date),
      value,
      id: 0
    };

    const filter = this.sytemNotes.filter(f=>{
      if (f.type === type) {
        return f;
      }
    });

    if (filter.length > 0) {
      this.sytemNotes = this.sytemNotes.map(m => {
        if (m.type === type) {
          obj.id = m.id;
          m = obj;
        }
        return m;
      });
    } else {
      obj.id = this.sytemNotes.length + 1;
      this.sytemNotes = [...this.sytemNotes, obj];
    }

    this.notes$.subscribe(res=>{
      this.combinedNotes = [...this.sytemNotes, ...res];
      this.combinedNotes = this.combinedNotes.sort((a, b)=>+ new Date(b.date) - + new Date(a.date));
    });

  }

  checkCurrentTime(){

    setInterval(()=>{
      this.time = new Date();
      this.calcWaktu();
      this.calcCurrentRemaining();
      this.calcElapsedTime();
    },1000);

  }

  stringToDate(time, date?){
    const split = time.split(':');
    let day;

    if (!date) {
      day = new Date();
    } else {
      day = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    }

    day.setHours(split[0]);
    day.setMinutes(split[1]);
    day.setSeconds(split[2]);
    return day;
  }

  calcWaktu() {

    if(this.time > this.jadualSemalam.isya && this.time < this.jadual.subuh) {
      this.currentWaktu.name = 'Isya';
      this.currentWaktu.time = this.jadualSemalam.isya;
      this.nextWaktu.name = 'Subuh';
      this.nextWaktu.time = this.jadual.subuh;
    }
    if(this.time > this.jadual.subuh && this.time < this.jadual.zohor) {
      this.currentWaktu.name = 'Subuh';
      this.currentWaktu.time = this.jadual.subuh;
      this.nextWaktu.name = 'Zohor';
      this.nextWaktu.time = this.jadual.zohor;
    }
    if(this.time > this.jadual.zohor && this.time < this.jadual.asar) {
      this.currentWaktu.name = 'Zohor';
      this.currentWaktu.time = this.jadual.zohor;
      this.nextWaktu.name = 'Asar';
      this.nextWaktu.time = this.jadual.asar;
    }
    if(this.time > this.jadual.asar && this.time < this.jadual.maghrib) {
      this.currentWaktu.name = 'Asar';
      this.currentWaktu.time = this.jadual.asar;
      this.nextWaktu.name = 'Maghrib';
      this.nextWaktu.time = this.jadual.maghrib;
    }
    if(this.time > this.jadual.maghrib && this.time < this.jadual.isya) {
      this.currentWaktu.name = 'Maghrib';
      this.currentWaktu.time = this.jadual.maghrib;
      this.nextWaktu.name = 'Isya';
      this.nextWaktu.time = this.jadual.isya;
    }
    if(this.time > this.jadual.isya && this.time < this.jadualEsok.subuh) {
      this.currentWaktu.name = 'Isya';
      this.currentWaktu.time = this.jadual.isya;
      this.nextWaktu.name = 'Subuh';
      this.nextWaktu.time = this.jadualEsok.subuh;
    }

  }

  calcCurrentRemaining() {

    const wow = intervalToDuration({
      start: this.time,
      end: this.nextWaktu.time
    });
    this.remainingTime = {...wow};

    const remainingNote = `${ this.remainingTime?.years ? this.remainingTime.years + ' Tahun ' : '' }${ this.remainingTime?.months ? this.remainingTime.months + ' Bulan ' : '' }${ this.remainingTime?.weeks ? this.remainingTime.weeks + ' Minggu ' : '' }${ this.remainingTime?.days ? this.remainingTime.days + ' Hari ' : '' }${ this.remainingTime?.hours ? this.remainingTime.hours + ' Jam ' : '' }${ this.remainingTime?.minutes ? this.remainingTime.minutes + ' Minit ' : '' }${ this.remainingTime?.minutes === '0' && this.remainingTime?.seconds ? this.remainingTime.seconds + ' Saat ' : '' } sehingga waktu ${ this.nextWaktu?.name }.`;
    this.newSystemNote('remaining', remainingNote, this.currentWaktu.time);
  }

  calcElapsedTime() {

    const wow = intervalToDuration({
      start: this.currentWaktu.time,
      end: this.time
    });
    this.elapsedTime = {...wow};

    const currentNote = `Sekarang adalah waktu ${ this.currentWaktu?.name } sejak ${ this.elapsedTime?.years ? this.elapsedTime.years + ' Tahun ' : '' }${ this.elapsedTime?.months ? this.elapsedTime.months + ' Bulan ' : '' }${ this.elapsedTime?.weeks ? this.elapsedTime.weeks + ' Minggu ' : '' }${ this.elapsedTime?.days ? this.elapsedTime.days + ' Hari ' : '' }${ this.elapsedTime?.hours ? this.elapsedTime.hours + ' Jam ' : '' }${ this.elapsedTime?.minutes ? this.elapsedTime.minutes + ' Minit ' : '' }${ this.elapsedTime?.minutes === '0' && this.elapsedTime?.seconds ? this.elapsedTime.seconds + ' Saat ' : '' } yang lalu.`;
    this.newSystemNote('current', currentNote, this.currentWaktu.time);
  }

  async createNoteModal() {
    const modal = await this.modalController.create({
      component: CreateNoteComponent
    });
    return await modal.present();
  }

  async editNoteModal(data) {
    const modal = await this.modalController.create({
      component: EditNoteComponent,
      componentProps: {
        noteData: data
      }
    });
    return await modal.present();
  }

  distanceToNow(val){
    return formatDistanceToNow(
      val instanceof Date ? val : parseISO(val),
      {
        includeSeconds: true,
        locale: ms
      }
    );
  }

  async presentActionSheet(i) {

    let options = [
      {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Edit',
        icon: 'create',
        handler: () => {
          this.editNoteModal(this.combinedNotes[i]);
        }
      }
    ];

    if (!this.combinedNotes[i].type) {
      options = [
        ...options,
        {
          text: 'Delete',
          role: 'destructive',
          icon: 'trash',
          handler: () => {
            this.notesService.deleteNote(this.combinedNotes[i].value);
          }
        }
      ];
    }

    const actionSheet = await this.actionSheetController.create({
      // header: 'Albums',
      buttons: [...options]
    });
    await actionSheet.present();

    const { role } = await actionSheet.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  identify(index, item) {
    return item.id;
  }
}
