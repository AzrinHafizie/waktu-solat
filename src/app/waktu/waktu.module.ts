import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WaktuPageRoutingModule } from './waktu-routing.module';

import { WaktuPage } from './waktu.page';
import { CreateNoteComponent } from './components/create-note/create-note.component';
import { EditNoteComponent } from './components/edit-note/edit-note.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WaktuPageRoutingModule
  ],
  declarations: [WaktuPage, CreateNoteComponent, EditNoteComponent]
})
export class WaktuPageModule {}
