import { Component } from '@angular/core';
import { Router, NavigationEnd} from '@angular/router';

import {  trigger,  state,  style,  animate,  transition } from '@angular/animations';
@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss'],
  animations: [
    trigger('slideUp', [
      transition(':enter', [
        style({ marginTop: '-44px' }),
        animate('100ms', style({ marginTop: '0' })),
      ]),
      transition(':leave', [
        animate('100ms', style({ marginTop: '-44px' }))
      ])
    ])
  ]

})
export class TabsPage {

  logoHidden = false;

  constructor(
    private router: Router,
  ) {

    this.router.events.subscribe((res: any)=>{
      if (res instanceof NavigationEnd){
        if ( res.url = '/tabs/waktu') {
          this.logoHidden = false;
        } else {
          this.logoHidden = true;
        }

      }

    });

  }

}
