import { TestBed } from '@angular/core/testing';

import { JakimService } from './jakim.service';

describe('JakimService', () => {
  let service: JakimService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JakimService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
