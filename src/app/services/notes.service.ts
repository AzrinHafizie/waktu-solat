import { Injectable } from '@angular/core';
import { BehaviorSubject, from } from 'rxjs';

import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class NotesService {

  private notes: BehaviorSubject<any> = new BehaviorSubject([]);
  public notes$ = this.notes.asObservable();

  constructor(
    private storageService: StorageService
  ) { }

  setNotes(val){
    this.notes.next(val);
  }

  createNotes(val){
    this.notes.next([...this.notes.value, val]);

    const notes = this.notes.value.sort((a,b)=>+ new Date(b.date) - + new Date(a.date));

    this.storageService.storage.set('notes', notes);

  }

  editNotes(val, prevValue){
    const arr = this.notes.value.map((f, idx) => {
      if (f.value === prevValue.value) {
        f = val;
      }
      return f;
    });
    this.notes.next([...arr]);
    this.storageService.storage.set('notes', arr);

  }

  deleteNote(value){
    const arr = this.notes.value.filter((f, idx) => {
      if (f.value != value) {
        return f;
      }
    });
    this.notes.next([...arr]);
    this.storageService.storage.set('notes', this.notes.value);
  }

}
