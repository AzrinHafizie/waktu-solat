import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BehaviorSubject } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class JakimService {

  baseUrl = 'https://www.e-solat.gov.my/index.php?r=esolatApi/takwimsolat';

  private yearlySchedule: BehaviorSubject<any> = new BehaviorSubject({});
  public yearlySchedule$ = this.yearlySchedule.asObservable();

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) { }

  getYearly(zone){
    return this.http.get(this.baseUrl+'&period=year&zone='+`${zone}`).pipe(
      tap(res => {
        this.yearlySchedule.next(res);
        this.storageService.storage.set('yearlySchedule', res);
        return res;
      }),
      catchError(err => err)
    );
  }

  setYearlySchedule(val){
    this.yearlySchedule.next(val);
  }

}
