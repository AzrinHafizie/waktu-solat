import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { StorageService } from './storage.service';
@Injectable({
  providedIn: 'root'
})
export class LocationService {

  private location: BehaviorSubject<any> = new BehaviorSubject({});
  public location$ = this.location.asObservable();

  constructor(
    private storageService: StorageService
  ) { }

  selectedLocation(){
    if (this.location.value && this.location.value.state) {
      this.location.next({...this.location.value});
    }

  }

  setLocation(val){
    this.location.next({...val});
    this.storageService.storage.set('location', val);
  }

}
