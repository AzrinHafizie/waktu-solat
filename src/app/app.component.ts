import { Component } from '@angular/core';

import { differenceInCalendarDays } from 'date-fns';

import { StorageService } from './services/storage.service';
import { LocationService } from './services/location.service';
import { JakimService } from './services/jakim.service';
import { NotesService } from './services/notes.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(
    private storageService: StorageService,
    private locationService: LocationService,
    private jakimService: JakimService,
    private notesService: NotesService,
    private loadingController: LoadingController

  ) {
    this.checkStorage();
  }

  checkStorage(){
    this.storageService.storage.get('location').then(res=>{
      if(res && res.code){
        this.locationService.setLocation(res);
      }
    });

    this.storageService.storage.get('yearlySchedule').then(res=>{
      let now = new Date()
      let currYear = now.getFullYear()
      let diff = 0
      let storedYear = new Date().getFullYear()
      let storedDate = new Date();

      if (res && res.serverTime) {
        storedDate = new Date(res.serverTime)
        storedYear = storedDate.getFullYear()
        diff = differenceInCalendarDays(now, storedDate)
        console.log(diff, storedYear);
      }

      if (currYear !== storedYear || diff > 7) {
        let loader = this.showLoadingIndictator()
        this.jakimService.getYearly(res.zone).subscribe(res => {
          loader.then(res=>{
            res.dismiss()
          })
        })
      }

      if(res && res.prayerTime && res.prayerTime.length>0){
        this.jakimService.setYearlySchedule(res);
      }
    });

    this.storageService.storage.get('notes').then(res=>{

      let resKey = [];
      if(res){
        resKey = Object.keys(res);
      }
      if(resKey.length>0){

        const notes = res.sort((a,b)=>+ new Date(b.date) - + new Date(a.date));

        this.notesService.setNotes(notes);
      }
    });
  }

  async showLoadingIndictator() {
    const loader = await this.loadingController.create({
      message: 'Pleases wait...',
     });
    await loader.present();
    return loader;
  }
}
